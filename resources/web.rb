#
# Cookbook:: foswiki
# Resource:: foswiki_web
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

resource_name 'foswiki_web'
unified_mode true if respond_to? :unified_mode

property :web_name, String, name_property: true

property :base, String, default: '_default'
property :color, String, default: '#efefef'
property :list_sitemap, [true, false], default: true
property :search_all, [true, false], default: true
property :summary, String, desired_state: false

property :user, String, default: 'root'
property :group, String, default: 'root'

# Manage webs in other Foswiki installations
property :data_dir, String
property :script_dir, String

action :create do
  res = new_resource
  args = "-baseweb '#{res.base}' -webbgcolor '#{res.color}'"
  args += ' -nosearchall on' unless res.search_all
  args += ' -sitemaplist on' if res.list_sitemap
  args += " -webbgcolor #{res.color}" unless res.color.nil?
  args += " -websummary '#{res.summary}'" unless res.summary.nil?

  execute "perl -CA manage -action createweb -newweb '#{res.web_name}' #{args} | grep -F new_web_has_been_created" do
    cwd real_script_dir
    user res.user
    group res.group
    not_if { ::Dir.exist? "#{real_data_dir}/#{res.web_name}" }
  end
end

action_class do
  include Foswiki::Helper

  def real_data_dir
    new_resource.data_dir || foswiki_dir('data')
  end

  def real_script_dir
    new_resource.script_dir || foswiki_dir('script')
  end
end
