#
# Cookbook:: foswiki
# Resource:: foswiki_configure
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

resource_name 'foswiki_configure'
unified_mode true if respond_to? :unified_mode

property :parameter, String, name_property: true

property :config, Hash, coerce: proc { |value|
  if value.is_a? Hash
    def stringify_keys(val)
      val.transform_keys!(&:to_s)
      val.each_value { |v| stringify_keys(v) if v.is_a? Hash }
    end
    stringify_keys(value)
  end
}
property :value, [Integer, String], default: 1

# Manage parameters in other Foswiki installations
property :tools_dir, String, desired_state: false

load_current_value do |desired|
  node = desired.run_context.node
  real_tools_dir = desired.tools_dir || Foswiki::Helper.foswiki_dir('tools', node)

  p = provider_for_action(:save)
  if desired.config
    config p.foswiki_config(desired.config || desired.parameter,
                            real_tools_dir)
  else
    value p.foswiki_get(desired.parameter, real_tools_dir) || ''
  end
  tools_dir real_tools_dir
end

action :save do
  converge_if_changed do
    new_config = new_resource.config || value_hash
    old_config = foswiki_config(new_config, real_tools_dir)
    old_config = foswiki_flat_config(old_config)
    new_config = foswiki_flat_config(new_config)

    diff_keys = new_config.keys.reject do |key|
      old_config[key] == new_config[key]
    end
    diff = diff_keys.map { |k| [k, new_config[k]] }.to_h

    set = diff.map do |key, value|
      "-set #{key}=#{value_to_string(value)}"
    end.join(' ')
    key_names = diff.keys.map { |k| k[1..-2].gsub('}{', '.') }.join(', ')

    execute "foswiki configure #{key_names}" do
      cwd real_tools_dir
      command "./configure -noprompt -save #{set} 2>&1"
      user node['foswiki']['user']
      not_if { set.empty? }
    end
  end
end

action_class do
  include Foswiki::Config
  include Foswiki::Helper

  def real_tools_dir
    new_resource.tools_dir || foswiki_dir('tools')
  end

  def value_hash
    if new_resource.parameter[0] != '{'
      return { new_resource.parameter => new_resource.value }
    end

    params = new_resource.parameter[1..-2].split('}{')
    hash = { params[-1] => new_resource.value }
    params[0..-2].reverse.each do |param|
      hash = { param => hash }
    end
    hash
  end

  def value_to_string(value)
    if value.is_a?(String) && value.delete("\n") =~ /^{.*}$/
      value = value.delete("\n").gsub(/\$/, '\$').gsub(/ +/, ' ')
      value = value.gsub(/^{\s*/, '{').gsub(/\s}$/, '}')
      return "\"#{value}\""
    end
    return "'#{value}'" if value.is_a? String
    return '0' if value == false
    return '1' if value == true
    if value.is_a? Array
      return "\"[#{value.map { |v| value_to_string(v) }.join(',')}]\""
    end
    value
  end
end
