#
# Cookbook:: foswiki
# Resource:: foswiki_install
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

resource_name 'foswiki_install'
unified_mode true if respond_to? :unified_mode

property :checksum, String, desired_state: false
property :force, [true, false], default: false, desired_state: false
property :install_dir, String, name_property: true, identity: true
property :url, String, desired_state: false
property :version, String, required: true, identity: true

load_current_value do |desired|
  current_version = Foswiki::Helper.foswiki_version(desired.install_dir)
  current_version ||= 'not installed'
  current_version = 'ignored (forced)' if desired.force
  version current_version
  install_dir desired.install_dir
end

action :install do
  converge_if_changed do
    current_version = foswiki_version(new_resource.install_dir)
    unless current_version.nil?
      Chef::Log.warn 'Use action :upgrade on foswiki_install to upgrade from ' \
        "#{current_version} to #{new_resource.version}"
      return
    end

    install
  end
end

action :upgrade do
  converge_if_changed do
    current_version = foswiki_version(new_resource.install_dir)
    new_resource.url ||= foswiki_download_url(new_resource.version, !current_version.nil?)
    install
  end
end

action_class do
  include Foswiki::Helper

  def install
    cache_path = Chef::Config[:file_cache_path]
    download_url = new_resource.url || foswiki_download_url(new_resource.version)
    download_url = download_url.sub(/{version}/, new_resource.version)
    archive_path = "#{cache_path}/Foswiki-#{new_resource.version}.tgz"

    remote_file archive_path do
      source download_url
      checksum new_resource.checksum if new_resource.checksum
      mode '0644'
      action :create_if_missing
    end

    directory new_resource.install_dir do
      recursive true
      user node['foswiki']['user']
      group node['foswiki']['group']
    end

    execute 'unpack Foswiki tar file' do
      cwd new_resource.install_dir
      command 'tar --extract --strip-components=1 ' \
        "--group '#{node['foswiki']['group']}' --file '#{archive_path}'"
      user node['foswiki']['user']
      group node['foswiki']['group']
    end

    execute 'bootstrap Foswiki configuration' do
      cwd new_resource.install_dir
      command 'perl -CAS tools/configure -save -noprompt'
      user node['foswiki']['user']
      group node['foswiki']['group']
    end
  end
end
