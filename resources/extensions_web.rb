#
# Cookbook:: foswiki
# Resource:: foswiki_extensions_web
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

resource_name 'foswiki_extensions_web'
unified_mode true if respond_to? :unified_mode

property :web_name, String, name_property: true

property :color, String, default: '#efefef'
property :list_sitemap, [true, false], default: false
property :search_all, [true, false], default: false

property :user, String, default: 'root'
property :group, String, default: 'root'

# Manage extension webs in other Foswiki installations
property :data_dir, String
property :install_dir, String
property :pub_url, String
property :script_dir, String
property :tools_dir, String
property :view_url, String

action :create do
  # Install needed plugin
  foswiki_plugin 'ExtensionsRepositoryContrib' do
    data_dir real_data_dir
    install_dir real_install_dir
    tools_dir real_tools_dir
    user new_resource.user
    group new_resource.group
  end

  # Create subweb
  foswiki_web new_resource.web_name do
    base '_ExtensionsRepository'
    color new_resource.color
    data_dir real_data_dir
    list_sitemap new_resource.list_sitemap
    script_dir real_script_dir
    search_all new_resource.search_all
    summary 'Custom extensions'
    user new_resource.user
    group new_resource.group
  end

  extensions_dir = "#{real_data_dir}/#{new_resource.web_name}"

  # Sadly the ExtensionsRepositoryContrib has not been updated since
  # 2010. Some files are therefor outdated and need to be fixed.

  # Create script needed to install extensions from this repository
  cookbook_file "#{extensions_dir}/JsonReport.txt" do
    source 'JsonReport.txt'
  end

  # Update package form
  cookbook_file "#{extensions_dir}/PackageForm.txt" do
    source 'PackageForm.txt'
  end

  # Add the local repository as a source for extensions
  foswiki_extensions_repository 'Local' do
    web_url "#{real_view_url}/#{new_resource.web_name}/"
    pub_url "#{real_pub_url}/#{new_resource.web_name}/"
  end
end

action_class do
  include Foswiki::Helper

  def real_data_dir
    new_resource.data_dir || foswiki_dir('data')
  end

  def real_install_dir
    new_resource.install_dir || foswiki_dir('install')
  end

  def real_pub_url
    new_resource.pub_url || foswiki_pub_url
  end

  def real_script_dir
    new_resource.script_dir || foswiki_dir('script')
  end

  def real_tools_dir
    new_resource.tools_dir || foswiki_dir('tools')
  end

  def real_view_url
    new_resource.view_url || foswiki_view_url
  end
end
