#
# Cookbook:: foswiki
# Resource:: foswiki_patches
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

resource_name 'foswiki_patches'
unified_mode true if respond_to? :unified_mode

property :group, String, default: 'www-data'
property :holidaylistplugin, [true, false], default: false
property :noproxy, [true, false], default: false
property :owner, String, default: 'www-data'
property :version, String, default: '2.1.6'

action :update do
  if new_resource.holidaylistplugin
    patch_file 'Foswiki/Plugins/HolidaylistPlugin/Core.pm', 'HolidaylistPluginCore.pm'
  end

  if new_resource.noproxy
    patch_file 'Foswiki/Net.pm', 'FoswikiNet.pm'
    patch_file 'Foswiki.spec', 'Foswiki.spec.erb', version: new_resource.version
  end
end

action_class do
  def patch_file(filename, asource, variables = nil)
    service 'apache2'

    type = variables.nil? ? :cookbook_file : :template
    declare_resource type, "#{foswiki_dir('install')}/lib/#{filename}" do
      source asource
      owner  new_resource.owner
      group new_resource.group
      mode '0444'
      variables variables unless variables.nil?
      notifies :restart, 'service[apache2]'
    end
  end
end
