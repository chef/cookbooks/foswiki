#
# Cookbook:: foswiki
# Resource:: foswiki_plugin
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

resource_name 'foswiki_plugin'
unified_mode true if respond_to? :unified_mode

property :plugin_name, String, name_property: true

property :enable, [true, false], default: true
property :force, [true, false], default: false
property :foswiki_version, String

property :user, String, default: 'root'
property :group, String, default: 'root'

# Manage plugins in other Foswiki installations
property :data_dir, String
property :install_dir, String
property :tools_dir, String

action :install do
  return if obsolete?

  install_dependencies

  enable_flag = new_resource.enable ? '' : ' -noenable'
  execute "#{installer} #{new_resource.plugin_name} install#{enable_flag} -r" do
    cwd real_install_dir
    user new_resource.user
    group new_resource.group
    only_if { new_resource.force || !installed? }
  end

  if new_resource.plugin_name =~ /Plugin$/
    foswiki_configure "{Plugins}{#{new_resource.plugin_name}}{Enabled}" do
      value new_resource.enable ? 1 : 0
      tools_dir real_tools_dir
    end
  end
end

action :uninstall do
  execute "#{installer} #new_resource.{plugin_name} uninstall" do
    cwd real_install_dir
    user new_resource.user
    group new_resource.group
    only_if { new_resource.force || installed? }
  end

  foswiki_configure "{Plugins}{#{new_resource.plugin_name}}{Enabled}" do
    value 0
  end
end

action_class do
  include Foswiki::Dependencies
  include Foswiki::Helper

  def install_dependencies
    package_manager = platform?('debian') ? 'apt' : nil
    return if package_manager.nil?

    dependencies = foswiki_plugin_dependencies(new_resource.plugin_name)

    return if dependencies.nil?

    (dependencies[package_manager.to_sym] || []).each { |p| package p }

    (dependencies[:foswiki] || []).each do |plugin|
      foswiki_plugin plugin do
        data_dir real_data_dir
        enable true if new_resource.enable
        force true if new_resource.force
        foswiki_version new_resource.foswiki_version
        group new_resource.group
        install_dir real_install_dir
        tools_dir real_tools_dir
        user new_resource.user
        action :install
      end
    end
  end

  def installed?
    ::File.exist?("#{real_data_dir}/System/#{new_resource.plugin_name}.txt")
  end

  def installer
    "#{real_tools_dir}/extension_installer"
  end

  def obsolete?
    return false if new_resource.foswiki_version.nil?
    foswiki_plugin_is_obsolete?(new_resource.plugin_name, new_resource.foswiki_version)
  end

  def real_data_dir
    new_resource.data_dir || foswiki_dir('data')
  end

  def real_install_dir
    new_resource.install_dir || foswiki_dir('install')
  end

  def real_tools_dir
    new_resource.tools_dir || foswiki_dir('tools')
  end
end
