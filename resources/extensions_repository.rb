#
# Cookbook:: foswiki
# Resource:: foswiki_extensions_repository
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

resource_name 'foswiki_extensions_repository'
unified_mode true if respond_to? :unified_mode

property :repo_name, String, name_property: true

property :web_url, String, required: true
property :pub_url, String, required: true

# Manage repositories in other Foswiki installations
property :tools_dir, String

action :save do
  foswiki_configure "Add extensions repository #{new_resource.repo_name}" do
    config(
      ExtensionsRepositories: (reduced_current_value << entry).sort.join(';')
    )
    tools_dir real_tools_dir
  end
end

action :delete do
  foswiki_configure "Remove extensions repository #{new_resource.repo_name}" do
    config(
      ExtensionsRepositories: reduced_current_value.join(';')
    )
    tools_dir real_tools_dir
  end
end

action_class do
  include Foswiki::Config
  include Foswiki::Helper

  def entry
    "#{new_resource.name}=(#{new_resource.web_url},#{new_resource.pub_url})"
  end

  def real_tools_dir
    new_resource.tools_dir || foswiki_dir('tools')
  end

  def reduced_current_value
    current_value = foswiki_get('ExtensionsRepositories', real_tools_dir)
    current_value.split(';')
                 .reject { |r| r.start_with? "#{new_resource.name}=" }
                 .uniq.sort
  end
end
