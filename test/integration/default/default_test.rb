# InSpec test for recipe foswiki::default

APACHE_PORT = 2406

describe port(APACHE_PORT) do
  it { should be_listening }
end

describe command('wget -qO /dev/stdout http://localhost:2406/') do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should match(/Welcome to the Main web/i) }
end

describe command('wget -qO /dev/stdout http://localhost:2406/System/WebHome') do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should match(/Welcome to the System web/i) }
end
