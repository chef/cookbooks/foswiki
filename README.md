# Foswiki Cookbook

This cookbook installs and configures [Foswiki](https://www.foswiki.org/).

## Requirements

Currently only Debian is supported. Apache is supported but does not have to be used.

## Installation

With the default recipe a single wiki instance will be installed. Fot this the attributes `['foswiki']['download_url']`, `['foswiki']['install_dir']` and `['foswiki']['version']` can be used.

For custom wiki installations use the `foswiki_install` resource.

```ruby
foswiki_install name do
  checksum
  force       # default: false
  install_dir # if not name
  url
  version     # required
end
```

## Configuration

The default wiki installation can be configured with attributes under `['foswiki']['config']`. For example `['foswiki']['config']['DataDir']` or `['foswiki']['config']['ScriptUrlPath']`. Values for xDir attributes can contain the substring "{install}" which will be replaced with `['foswiki']['install_dir']`. Also there is a `foswiki_configure` resource. Values can be retrieved from Chef Vault. Just use the the format `{vault:#{vault}:#{item}:#{value}}` in values. Hash values have to be encoded as a string, for example `'{\'a\' => \'b\'}'`.

```ruby
foswiki_configure "{Path}{To}{Parameter}" do
  parameter # if not name
  value     # string or integer
  tools_dir # default: from default installation
end

# or

foswiki_configure name do
  config    # hash of parameters and values
  tools_dir # default: from default installation
end
```

## Webs

You can add a web with the `foswiki_web` resource:

```ruby
foswiki_web name do
  web_name     # if not name
  base         # default: _defaul)
  color        # default: #efefef
  list_sitemap # list web on sitemap, default: true
  search_all   # use web in global search, default: true
  summary      # string
  user         # default: root
  group        # default: root
  data_dir     # default from default installation
  script_dir   # default: from default installation
end
```

## Topics

The default recipe can create topics and thereby remove their existing history.

### System.SitePreferences

You can configure site preferences with attributes under `['foswiki']['site_preferences']`. To disable management of the topic set `['foswiki']['site_preferences']['disable']` to `true`.

For custom installations you declare a Chef template using `SitePreferences.txt.erb` as source.

### WEB.WebPreferences

Web preferences can be configured under `['foswiki']['web_preferences'][web]`. The `System` web is predefined. To disable management of the topics set `['foswiki']['web_preferences']['disable']` for all or `['foswiki']['web_preferences'][web]['disable']` for a specific web.

For custom installations you decare Chef templates using `WebPreferences.txt.erb` as source.

### Other topics

You can declare raw topics under `['foswiki']['topics'][topic] = 'content'`. Topic names can use slashes or dots as seperators between webs and topics.

For custom installations just use Chef's file resource. Be sure to change the modification date of the file if the correct history matters to you.

## Plugins

Every plugin in the list `['foswiki']['plugins']` will be added to the default wiki installation.

For custom installations you can use the `foswiki_plugin` resource:

```ruby
foswiki_plugin name do
  plugin_name     # if not name
  enable          # default: true
  force           # default: false
  foswiki_version # used to skip obsolete plugins
  user            # used to execute commands, default: roo)
  group           # used to execute commands, default: root
  data_dir        # default: from default installation
  install_dir     # default: from default installation
  tools_dir       # default: from default installation
end
```

## Extensions Repository

To use Foswiki plugins from sites other then foswiki.org, you can use the attribute `['foswiki']['config']['ExtensionsRepositories']` or the `foswiki_extensions_repository` resource.

```ruby
foswiki_extensions_repository name do
  repo_name # if not name
  web_url   # URL to the repositories web, required
  pub_url   # URL to the repositories web public area, required
  action    # :save (default) or :delete
end
```

## Extensions Web

A self hosted extensions web can be created by setting `['foswiki']['extensions_web']` to a name or by using the `foswiki_extensions_web` resource.

```ruby
foswiki_extensions_web name do
  web_name     # if not name
  color        # string, default: #efefef
  list_sitemap # list web on sitemap, default: false
  search_all   # use web in global search, default: false
  user         # default: root
  group        # default: root
  data_dir     # default: from default installation
  install_dir  # default: from default installation
  pub_url      # default: from default installation
  script_dir   # default: from default installation
  tools_dir    # default: from default installation
  view_url     # default: from default installation
end
```

## Unofficial patches

This cookbook provides some unofficial patches for Foswiki which can be controlled with attributes under `['foswiki']['patches']` or a `foswiki_patches` resource.

```ruby
foswiki_patches name do
  holidaylistplugin # Make HolidaylistPlugin compatible with newer Perl versions and add option "priopubholidays"
  noproxy           # Add the configuration item {PROXY}{NoProxy} (https://foswiki.org/Development/AddNoProxyFeature)
  owner             # owner of patched files
  group             # used group for patched files
end
```

Moreover if `['foswiki']['config']['Cache']['Enabled']` is set to `true`, a script called `tidy_page_cache.sh` is installed under the tools directory. This can be used to remove orphaned or unnecessary entries. With `['foswiki']['cron']['tidy_page_cache_time']` the script can be run regularly.

## robots.txt

Foswiki's robots.txt can be configured with `['foswiki']['robots']['crawl_delay']` (Integer) and `['foswiki']['robots']['disallow']` (List). For custom installations the template `templates/robots.txt.erb` can be used.

## Apache configuration

Optionally you can use the `foswiki::apache` recipe. See `attributes/apache.rb` for attributes which are used in it. It only includes the apache2 cookbook and configures a template and apache2_site resource. The template `templates/apache2_site.conf.erb` can be used in custom wiki installations, as well.

The recipe automatically configures FCGI and XSendFile if the plugins [FastCGIEngineContrib](https://foswiki.org/Extensions/FastCGIEngineContrib) or [XSendFilePlugin](https://foswiki.org/Extensions/XSendFileContrib) are listed in `['foswiki']['plugins']`.
