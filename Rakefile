begin
  require 'cookstyle'
  require 'rubocop/rake_task'

  desc 'Run Cookstyle checks'
  RuboCop::RakeTask.new(:cookstyle)
rescue LoadError
  desc 'rubocop rake task not available'
  task :cookstyle do
    abort 'Rubocop/Cookstyle rake task is not available. Be sure to install rubocop and cookstyle'
  end
end

begin
  require 'rspec/core/rake_task'
  RSpec::Core::RakeTask.new(:chefspec) do |task|
    task.pattern = 'test/unit/**/*_spec.rb'
    task.rspec_opts = '--backtrace --color --format documentation '\
                      '--require spec_helper --default-path test/unit'
  end
rescue LoadError
  desc 'chefspec rake task not available'
  task :chefspec do
    abort 'Chefspec rake task is not available. Be sure to install chefspec'
  end
end

begin
  require 'kitchen/rake_tasks'
  Kitchen::RakeTasks.new do
    Kitchen.logger = Kitchen.default_logger
  end
rescue LoadError
  desc 'kitchen rake task not available'
  task :kitchen do
    abort 'kitchen rake task is not available. Be sure to install test-kitchen'
  end
end

begin
  require 'yard'
  YARD::Config.load_plugin 'chef'
  YARD::Rake::YardocTask.new do |t|
    t.files = ['**/*.rb']
    t.options = ['--debug']
  end
rescue LoadError
  desc 'yard rake task not available'
  task :yard do
    abort 'yard rake task is not available. '\
          'Be sure to install yard and yard-chef'
  end
end

task default: %w(cookstyle chefspec kitchen:all)
