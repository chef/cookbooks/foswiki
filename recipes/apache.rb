#
# Cookbook:: foswiki
# Recipe:: apache
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Initialize data from apache2 cookbook
Chef::DSL::Recipe.include Apache2::Cookbook::Helpers
Chef::Resource.include Apache2::Cookbook::Helpers
apache_user = default_apache_user
apache_group = default_apache_group

service 'apache2' do
  service_name lazy { apache_platform_service_name }
  supports restart: true, status: true, reload: true
  action :nothing
end

apache2_install 'default_install' do
  modules []
end

# Some plugins hint to what Apache should do
if find_resource(:foswiki_plugin, 'XSendFileContrib')
  node.default['foswiki']['apache']['use_xsendfile'] = true
end
if find_resource(:foswiki_plugin, 'FastCGIEngineContrib')
  node.default['foswiki']['apache']['use_fcgi'] = true
end

apache_conf = node['foswiki']['apache'].dup
apache_conf['unprotected_attachments'] = [
  '[^/]+\.(gif|jpe?g|ico)$',
  'System/(.*)$',
  '([^/]+/)+WebPreferences/([^/]+)$',
] if apache_conf['unprotected_attachments'].eql? :default

# Configure mod_expires
apache2_module 'expires' if apache_conf['pub_expires_time']

# Configure mod_fcgid
if apache_conf['use_fcgi']
  apache2_mod_fcgid 'fcgid'
  edit_resource(:foswiki_plugin, 'FastCGIEngineContrib') do
    foswiki_version node['foswiki']['version']
    user apache_user
    group apache_group
    action :install
  end
  foswiki_configure '{Htpasswd}{GlobalCache}'
end

# Configure mod_xsendfile
if apache_conf['use_xsendfile']
  apache2_module 'xsendfile'
  edit_resource(:foswiki_plugin, 'XSendFileContrib') do
    foswiki_version node['foswiki']['version']
    user apache_user
    group apache_group
    action :install
  end
  foswiki_configure '{XSendFileContrib}{Header}' do
    value 'X-Sendfile'
  end
end

# Configure mod_ssl
apache2_mod_ssl 'ssl' if apache_conf['use_tls']

# Collect data for Apache site
dirs = {}
%w(data install locales pub script template tools).each do |dir|
  dirs[dir] = foswiki_dir(dir)
end
if apache_conf['favicon']
  favicon_path = foswiki_dir('pub') + apache_conf['favicon']
end
pub_url_path = node['foswiki']['config']['PubUrlPath']
script_url_path = node['foswiki']['config']['ScriptUrlPath']
script_url_path_view = node['foswiki']['config']['ScriptUrlPaths']['view']

template "#{apache_conf['server_name']}.conf" do
  source 'apache2_site.conf.erb'
  path "#{apache_dir}/sites-available/#{apache_conf['server_name']}.conf"
  variables(
    access_control: apache_conf['access_control'],
    blocked_agents: apache_conf['blocked_agents'],
    blocked_ips: apache_conf['blocked_ips'],
    cert_name: apache_conf['cert_name'],
    cert_ca_path: apache_conf['cert_ca_path'],
    cert_chain_file: apache_conf['cert_chain_file'],
    cert_file: apache_conf['cert_file'],
    cert_key_file: apache_conf['cert_key_file'],
    custom_log: apache_conf['custom_log'],
    data_dir: dirs['data'],
    favicon: favicon_path,
    fcgid_max_request_len: apache_conf['fcgid_max_request_len'],
    http_port: apache_conf['http_port'],
    https_port: apache_conf['https_port'],
    install_dir: dirs['install'],
    locales_dir: dirs['locales'],
    log_level: apache_conf['log_level'],
    protect_attachments: apache_conf['protect_attachments'],
    pub_dir: dirs['pub'],
    pub_expires_match: apache_conf['pub_expires_match'],
    pub_expires_time: apache_conf['pub_expires_time'],
    pub_url_path: pub_url_path,
    script_dir: dirs['script'],
    script_url_path: script_url_path,
    script_url_path_view: script_url_path_view,
    server_admin: apache_conf['server_admin'],
    server_alias: apache_conf['server_alias'],
    server_name: apache_conf['server_name'],
    template_dir: dirs['template'],
    tools_dir: dirs['tools'],
    unprotected_attachments: apache_conf['unprotected_attachments'],
    use_fcgi: apache_conf['use_fcgi'],
    use_short_urls: apache_conf['use_short_urls'],
    use_tls: apache_conf['use_tls'],
    use_xsendfile: apache_conf['use_xsendfile']
  )
end

apache2_site apache_conf['server_name'] do
  action :enable
end
