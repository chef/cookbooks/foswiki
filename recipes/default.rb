#
# Cookbook:: foswiki
# Recipe:: default
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Initialize data from apache2 cookbook
Chef::DSL::Recipe.include Apache2::Cookbook::Helpers
node.default_unless['foswiki']['user'] = default_apache_user
node.default_unless['foswiki']['group'] = default_apache_group

Chef::DSL::Recipe.include Foswiki::Dependencies

# Install needed packages
if platform_family? 'debian'
  packagelist = foswiki_apt_dependencies
else
  Chef::Log.error 'foswiki cookbook only supports debian family'
  return
end
packagelist.each { |pkg| package pkg }

current_version = foswiki_version
wanted_version = node['foswiki']['version']
unless wanted_version.eql?(current_version)
  if current_version.nil?
    Chef::Log.warn "Install Foswiki version #{wanted_version}"
  else
    Chef::Log.warn "Update Foswiki from #{current_version} to #{wanted_version}"
  end
end

install_dir = foswiki_dir('install')
tools_dir = foswiki_dir('tools')

# Install Foswiki from attributes
foswiki_install install_dir do
  url node['foswiki']['download_url']
  version node['foswiki']['version']
  action node['foswiki']['upgrade'] ? :upgrade : :install
end

template "#{foswiki_dir('install')}/robots.txt" do
  source 'robots.txt.erb'
  variables(
    crawl_delay: node['foswiki']['robots']['crawl_delay'],
    script_url_path: node['foswiki']['config']['ScriptUrlPath'],
    disallow: node['foswiki']['robots']['disallow'],
    disallow_agents: node['foswiki']['robots']['disallow_agents']
  )
end

# Configure Foswiki
new_config = node['foswiki']['config'].dup || {}
new_config.keys.select { |k| k.end_with?('Dir') }.each do |key|
  new_config[key] = new_config[key].gsub('{install}', install_dir)
end
unless new_config['PROXY'].nil? || new_config['PROXY']['HOST'].nil?
  # Proxy is important to install plugins later
  foswiki_configure '{PROXY}{HOST}' do
    value new_config['PROXY']['HOST']
    tools_dir tools_dir
  end
end
unless new_config['ExtensionsRepositories'].nil?
  new_config['ExtensionsRepositories'].each do |repo|
    name, url = repo.split('=(', 2)
    wurl, purl = url.split(',', 2)
    foswiki_extensions_repository name do
      web_url wurl
      pub_url purl.chomp(')')
    end
  end
  new_config.delete('ExtensionsRepositories')
end
foswiki_configure 'foswiki' do
  config new_config
  tools_dir tools_dir
end

# Create a default htpasswd file
htpasswd_file = foswiki_resolve(node['foswiki']['config']['Htpasswd']['FileName'])
htpasswd_file ||= foswiki_resolve(foswiki_get('{Htpasswd}{FileName}'))
htpasswd_file ||= "#{foswiki_dir('data')}/.htpasswd"
execute "htpasswd -b -c '#{htpasswd_file}' admin secret" do
  creates htpasswd_file
  user node['foswiki']['user']
  group node['foswiki']['group']
  ignore_failure true # Apache is not installed
end

# Manage AdminGroup
unless node['foswiki']['admin_group'].nil?
  template "#{foswiki_dir('data')}/Main/AdminGroup.txt" do
    source 'AdminGroup.txt.erb'
    variables(group: node['foswiki']['admin_group'] || [])
  end
  directory "#{foswiki_dir('data')}/Main/AdminGroup,pfv" do
    recursive true
    action :delete
  end
end

# Manage Main.SitePreferences
unless node['foswiki']['site_preferences']['disable']
  template "#{foswiki_dir('data')}/Main/SitePreferences.txt" do
    source 'SitePreferences.txt.erb'
    variables(config: node['foswiki']['site_preferences'] || {})
  end
  directory "#{foswiki_dir('data')}/Main/SitePreferences,pfv" do
    recursive true
    action :delete
  end
end

# Manage %WEB%.WebPreferences
unless node['foswiki']['web_preferences']['disable']
  (node['foswiki']['web_preferences'] || {}).each do |web, preferences|
    template "#{foswiki_dir('data')}/#{web}/WebPreferences.txt" do
      source 'WebPreferences.txt.erb'
      variables(config: preferences || {})
      not_if { preferences['disable'] }
    end
    directory "#{foswiki_dir('data')}/#{web}/WebPreferences,pfv" do
      recursive true
      action :delete
      not_if { preferences['disable'] }
    end
  end
end

# Manage other topics
(node['foswiki']['topics'] || {}).each do |topic, tcontent|
  base = "#{foswiki_dir('data')}/#{topic.tr('.', '/')}"
  file "#{base}.txt" do
    content tcontent
    user node['foswiki']['user']
    group node['foswiki']['group']
  end
  begin
    time = tcontent.split("\n")[0].split.grep(/^date=/)[0].split('"')[1]
    execute "Set mod time for #{base}.txt" do
      command "touch -m -d '@#{time}' '#{base}.txt'"
    end
  rescue NoMethodError
    Chef::Log.warn("Foswiki topic #{topic} has no modification date")
  end
  directory "#{base},pfv" do
    recursive true
    action :delete
  end
end

# Install plugins
(node['foswiki']['plugins'] || []).each do |plugin|
  foswiki_plugin plugin do
    foswiki_version node['foswiki']['version']
    user node['foswiki']['user']
    group node['foswiki']['group']
  end
end

# Create extensions web
unless node['foswiki']['extensions_web'].nil?
  foswiki_extensions_web node['foswiki']['extensions_web'] do
    user node['foswiki']['user']
    group node['foswiki']['group']
  end
end

# Apply patches which are not available in upstream
foswiki_patches 'default' do
  holidaylistplugin node['foswiki']['patches']['holidaylistplugin']
  noproxy node['foswiki']['patches']['noproxy']
  owner node['foswiki']['user']
  group node['foswiki']['group']
  version node['foswiki']['version']
end

# Manage Cron job
template '/etc/cron.d/foswiki' do
  source 'foswiki-cron.erb'
  mode '0644'
  variables(
    actionnotify_entries: node['foswiki']['cron']['actionnotify_entries'],
    entries: node['foswiki']['cron']['entries'],
    install_dir: foswiki_dir('install'),
    mailnotify_time: node['foswiki']['cron']['mailnotify_time'],
    mailto: node['foswiki']['cron']['mailto'],
    script_dir: foswiki_dir('script'),
    statistics_time: node['foswiki']['cron']['statistics_time'],
    tick_time: node['foswiki']['cron']['tick_time'],
    tidy_page_cache_max_age: node['foswiki']['cron']['tidy_page_cache_max_age'],
    tidy_page_cache_time: if node['foswiki']['config']['Cache']['Enabled']
                            node['foswiki']['cron']['tidy_page_cache_time']
                          end,
    tools_dir: foswiki_dir('tools'),
    trash_cleanup_time: node['foswiki']['cron']['trash_cleanup_time'],
    user: node['foswiki']['user']
  )
end

include_recipe 'foswiki::_cache'
