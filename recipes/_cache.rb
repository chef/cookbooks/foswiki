#
# Cookbook:: foswiki
# Recipe:: _cache
#
# Copyright:: 2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

Chef::DSL::Recipe.include Foswiki::Helper

username = 'foswiki'
password = ''
database = 'foswiki'

file "#{foswiki_dir('tools')}/mysql.cnf" do
  action :delete
end

case node['foswiki']['config']['Cache']['Implementation']
when 'Foswiki::PageCache::DBI::MySQL', 'Foswiki::PageCache::DBI::MariaDB'
  package 'libdbd-mysql-perl'

  impl = node['foswiki']['config']['Cache']['Implementation'].split(/:/)[-1]
  params = node['foswiki']['config']['Cache']['DBI'][impl]

  if %w(localhost 127.0.0.1).include? params['Host']
    package 'default-mysql-server'

    username = foswiki_secret(params['Username'])
    password = foswiki_secret(params['Password']).dup
    database = foswiki_secret(params['Database'])

    escaped_password = password.gsub('$', '\\$')
    escaped_password.gsub!('`', '\\`')

    execute 'initialize page cache database' do
      command "echo \"CREATE DATABASE IF NOT EXISTS \\`#{database}\\` CHARACTER SET = 'utf8'; " \
        "GRANT ALL PRIVILEGES ON \\`#{database}\\`.* TO '#{username}'@" \
        "'#{params['Host']}' IDENTIFIED BY '#{escaped_password}';\" | mysql -u root"
      sensitive true
    end

  else
    package 'default-mysql-client'
  end

  q = password.include?('"') ? "'" : '"'
  edit_resource :file, "#{foswiki_dir('tools')}/mysql.cnf" do
    content "[mysql]\nhost=#{params['Host']}\nport=#{params['Port'] || 3306}\n" \
      "user=#{username}\npassword=#{q}#{password}#{q}\n"
    mode '0440'
    owner node['foswiki']['user']
    group node['foswiki']['group']
    sensitive true
    action :create
  end

when 'Foswiki::PageCache::DBI::SQLite'
  package 'libdbd-sqlite3-perl'

  database = node['foswiki']['config']['Cache']['DBI']['SQLite']['Filename'] ||
             "#{foswiki_dir('working')}/sqlite.db"

end

template "#{foswiki_dir('tools')}/tidy_page_cache.sh" do
  source 'tidy_page_cache.sh.erb'
  variables(
    cache_dir: "#{foswiki_dir('working')}/cache",
    cache_implementation: node['foswiki']['config']['Cache']['Implementation'],
    database: database,
    mysql_cnf: "#{foswiki_dir('tools')}/mysql.cnf",
    data_dir: foswiki_dir('data'),
    table_prefix: node['foswiki']['config']['Cache']['DBI']['TablePrefix']
  )
  mode '0770'
  owner node['foswiki']['user']
  group node['foswiki']['group']
  action :delete unless node['foswiki']['config']['Cache']['Enabled']
end
