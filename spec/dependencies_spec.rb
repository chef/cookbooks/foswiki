require 'spec_helper'
require_relative '../libraries/dependencies'

describe Foswiki::Dependencies do
  describe '#foswiki_plugin_is_independent?("TrashPlugin")' do
    subject do
      Foswiki::Dependencies.foswiki_plugin_is_independent?('TrashPlugin')
    end
    it { should be true }
  end

  describe '#foswiki_plugin_is_independent?("NonExistingPlugin")' do
    subject do
      Foswiki::Dependencies.foswiki_plugin_is_independent?('NonExistPlugin')
    end
    it { should be false }
  end

  describe '#foswiki_plugin_is_obsolete?("SetVariablePlugin", "1.1")' do
    subject do
      Foswiki::Dependencies.foswiki_plugin_is_obsolete?('SetVariablePlugin', '1.1')
    end
    it { should be false }
  end

  describe '#foswiki_plugin_is_obsolete?("SetVariablePlugin", "2.1")' do
    subject do
      Foswiki::Dependencies.foswiki_plugin_is_obsolete?('SetVariablePlugin', '2.1')
    end
    it { should be true }
  end

  describe '#foswiki_plugin_dependencies("SubscribePlugin")' do
    subject do
      Foswiki::Dependencies.foswiki_plugin_dependencies('SubscribePlugin')
    end
    it { expect(subject).to eq({ apt: %w(libjson-xs-perl), foswiki: %w(MailerContrib) }) }
  end

  describe '#foswiki_plugin_dependencies(%w(SubscribePlugin WebFontsContrib))' do
    subject do
      Foswiki::Dependencies.foswiki_plugin_dependencies(%w(SubscribePlugin WebFontsContrib))
    end
    it { expect(subject).to eq({ apt: %w(libjson-xs-perl), foswiki: %w(JQueryPlugin MailerContrib) }) }
  end
end
