module Foswiki
  # Helper functions for the Foswiki cookbook
  module Helper
    module_function

    # Return a Foswiki directory (tools, lib, etc.)
    def foswiki_dir(name, node_obj = nil)
      node_obj ||= node
      return node_obj['foswiki']['install_dir'] if name.eql? 'install'
      dir = if name.eql? 'log'
              node_obj['foswiki']['config']['Log']['Dir']
            else
              node_obj['foswiki']['config']["#{name.capitalize}Dir"]
            end
      return if dir.nil?
      dir = dir.dup
      while dir =~ /\$Foswiki::cfg{/
        dir.gsub!(/\$Foswiki::cfg{([\w}{]+)}/) do |_|
          full_key = Regexp.last_match[1]
          value = node_obj['foswiki']['config']
          full_key.split('}{').each do |key|
            value = value[key]
            if value.nil?
              Chef::Log.error "Can not evaluate {#{full_key}} in #{name} dir"
              return dir
            end
          end
          value.to_s
        end
      end
      dir.gsub(/{install}/, node_obj['foswiki']['install_dir'])
    end

    # Return the url to the tar archive of Foswiki
    def foswiki_download_url(version, upgrade = false)
      return node['foswiki']['download_url'].sub('{version}', version) \
        unless node['foswiki']['download_url'].nil?
      'https://master.dl.sourceforge.net/project/foswiki/foswiki/' \
        "#{version}/Foswiki#{'-upgrade' if upgrade}-#{version}.tgz"
    end

    # Return the canonical URL to public resources
    def foswiki_pub_url(suffix = '', conf = nil)
      conf ||= node['foswiki']['config']
      "#{conf['DefaultUrlHost']}#{conf['PubUrlPath']}#{suffix}"
    end

    # Retrieve values from Chef Vault
    def foswiki_secret(str)
      return str unless str.is_a? String
      return str unless str =~ /{vault:[^:]+:[^:]+:[^:]+}/

      require 'chef-vault'
      str.sub(/{vault:[^:]+:[^:]+:[^:]+}/) do |secret|
        values = secret[1..-2].split(':')
        vault = ChefVault::Item.load(values[1], values[2])
        vault.nil? ? nil : vault[values[3]]
      end
    end

    # Return the canonical URL to view topics
    def foswiki_view_url(suffix = '', conf = nil)
      conf ||= node['foswiki']['config']
      "#{conf['DefaultUrlHost']}#{conf['ScriptUrlPaths']['view']}#{suffix}"
    end

    # Return the currently installed version
    def foswiki_version(install_dir = nil)
      install_dir ||= foswiki_dir('install')
      lines = ::File.read("#{install_dir}/lib/Foswiki.pm").split("\n")
      version = lines.grep(/\$VERSION\s*=/)[0].sub(/^.*=\s*/, '')
      version.sub(/^.*?['"](Foswiki-)?v?/, '').sub(/['"].*$/, '')
    rescue Errno::ENOENT then nil
    end
  end
end

Chef::DSL::Recipe.include Foswiki::Helper
Chef::Resource.include Foswiki::Helper
