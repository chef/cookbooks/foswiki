module Foswiki
  # Required dependencies for Foswiki plugins
  module Dependencies
    DEPENDENCIES = {
      ActionTrackerPlugin: {
        apt: %w(libjson-xs-perl libtext-soundex-perl libtime-parsedate-perl),
        foswiki: %w(JQueryPlugin),
      },
      BlogAddOn: {
        foswiki: %w(CommentPlugin SpreadSheetPlugin ZonePlugin),
      },
      BreadCrumbsPlugin: {
        foswiki: %w(TopicTitlePlugin),
      },
      CalendarPlugin: {
        apt: %w(libdate-calc-xs-perl libhtml-calendarmonthsimple-perl),
      },
      CaptchaPlugin: {
        apt: %w(libgd-perl libjson-xs-perl),
        foswiki: %w(JQueryPlugin JsonRpcContrib),
      },
      ChartPlugin: {
        apt: %w(libfile-path-tiny-perl libgd3 libgd-perl),
      },
      ConfigurePlugin: {
        foswiki: %w(JQueryPlugin JsonRpcContrib),
      },
      DBCacheContrib: {
        apt: %w(libclass-std-storable-perl libtime-parsedate-perl),
      },
      DBCachePlugin: {
        apt: %w(libclass-std-storable-perl),
        foswiki: %w(DBCacheContrib TopicTitlePlugin),
      },
      DiffPlugin: {
        apt: %w(libalgorithm-diff-xs-perl),
      },
      DirectedGraphPlugin: {
        apt: %w(graphviz libclass-std-storable-perl libdigest-perl-md5-perl libfile-spec-native-perl),
      },
      EditChapterPlugin: {
        foswiki: %w(JQueryPlugin JsonRpcContrib RenderPlugin),
      },
      ExcelImportExportPlugin: {
        apt: %w(libdate-manip-perl libspreadsheet-parseexcel-perl libspreadsheet-writeexcel-perl),
      },
      # File::Copy, File::Path, File::Temp ?
      FastCGIEngineContrib: {
        apt: %w(libfcgi-perl),
      },
      FlexWebListPlugin: {
        foswiki: %w(TopicTitlePlugin),
      },
      FormPlugin: {
        apt: %w(libjson-xs-perl liblist-moreutils-perl libregexp-common-perl),
        foswiki: %w(JQueryPlugin),
      },
      HolidaylistPlugin: {
        apt: %w(libdate-calc-perl),
        foswiki: %w(SmiliesPlugin),
      },
      ImagePlugin: {
        apt: %w(libimage-magick-perl liblwp-useragent-chicaching-perl),
        foswiki: %w(CacheContrib JQueryPlugin),
      },
      JEditableContrib: {
        foswiki: %w(JQueryPlugin),
      },
      JQSelect2Contrib: {
        foswiki: %w(JQueryPlugin),
      },
      JQueryPlugin: {
        apt: %w(libjson-xs-perl),
        foswiki: %w(ZonePlugin),
      },
      JSCalendarContrib: {
        foswiki: %w(JQueryPlugin),
      },
      JsonRpcContrib: {
        apt: %w(libjson-xs-perl),
      },
      JSTreeContrib: {
        foswiki: %w(JQueryPlugin),
      },
      KinoSearchContrib: {
        apt: %w(liberror-perl libkinosearch1-perl),
        foswiki: %w(StringifierContrib),
      },
      KinoSearchPlugin: {
        foswiki: %w(KinoSearchContrib),
      },
      LatexModePlugin: {
        apt: %w(dvipng imagemagick libdigest-perl-md5-perl libimage-info-perl texlive-latex-base),
      },
      LdapContrib: {
        apt: %w(libdb-file-lock-perl libdigest-perl-md5-perl libnet-ldap-perl libnet-ldap-perl libtext-unidecode-perl),
      },
      ListyPlugin: {
        foswiki: %w(FilterPlugin JQueryPlugin JsonRpcContrib RenderPlugin TopicTitlePlugin ZonePlugin),
      },
      MarkdownPlugin: {
        apt: %w(pandoc),
      },
      MathModePlugin: {
        apt: %w(dvipng imagemagick texlive-latex-base texlive-latex-recommended),
      },
      MetaCommentPlugin: {
        foswiki: %w(ImagePlugin JsonRpcContrib RenderPlugin TopicTitlePlugin ZonePlugin),
      },
      MetaDataPlugin: {
        foswiki: %w(FlexFormPlugin JQDataTablesPlugin JQueryPlugin JsonRpcContrib TopicTitlePlugin),
      },
      ModPerlEngineContrib: {
        apt: %w(libapache2-mod-perl2 libapache2-request-perl),
      },
      MoreFormfieldsPlugin: {
        apt: %w(libjson-xs-perl libscalar-list-utils-perl),
        foswiki: %w(DBCachePlugin ImagePlugin JQSelect2Contrib JQueryPlugin RenderPlugin TopicInteractionPlugin TopicTitlePlugin),
      },
      MultiLingualPlugin: {
        foswiki: %w(MetaDataPlugin),
      },
      NatEditPlugin: {
        apt: %w(libjson-xs-perl),
        foswiki: %w(JQueryPlugin SetVariablePlugin ZonePlugin),
      },
      NatSkin: {
        foswiki: %w(AutoTemplatePlugin BreadCrumbsPlugin CopyContrib DBCachePlugin DiffPlugin
       FamFamFamContrib FilterPlugin FlexFormPlugin FlexWebListPlugin
       GridLayoutPlugin ImagePlugin JQSelect2Contrib JQueryPlugin ListyPlugin
       MimeIconPlugin MoreFormfieldsPlugin MultiLingualPlugin NatEditPlugin
       RedDotPlugin RenderPlugin TopicInteractionPlugin TopicTitlePlugin
       WebFontsContrib WebLinkPlugin ZonePlugin),
      },
      NatSkinPlugin: {
        foswiki: %w(AutoTemplatePlugin BreadCrumbsPlugin CopyContrib DBCachePlugin DiffPlugin
       FamFamFamContrib FilterPlugin FlexFormPlugin FlexWebListPlugin
       GridLayoutPlugin ImagePlugin JQSelect2Contrib JQueryPlugin ListyPlugin
       MimeIconPlugin MoreFormfieldsPlugin MultiLingualPlugin NatEditPlugin
       RedDotPlugin RenderPlugin TopicInteractionPlugin TopicTitlePlugin
       WebFontsContrib WebLinkPlugin ZonePlugin),
      },
      PlainFileStoreContrib: {
        apt: %w(libfile-copy-recursive-perl),
      },
      PublishPlugin: {
        apt: %w(htmldoc libfile-spec-native-perl libarchive-zip-perl),
      },
      QueryAcceleratorPlugin: {
        foswiki: %w(DBCacheContrib),
      },
      RCSStoreContrib: {
        apt: %w(libfile-copy-recursive-perl),
      },
      RedDotPlugin: {
        foswiki: %w(JQueryPlugin),
      },
      RenderPlugin: {
        foswiki: %w(JQueryPlugin),
      },
      SpreadsheetReaderPlugin: {
        apt: %w(libspreadsheet-parseexcel-perl libspreadsheet-parsexlsx-perl libspreadsheet-read-perl libspreadsheet-readsxc-perl libtext-csv-xs-perl),
      },
      StringifierContrib: {
        # antiword could be replaced by soffice, abiword or wvWare
        # html2text could be replaced by lynx
        # libspreadsheet-parsexlsx-perl could be replaced by xlsx2csv
        # Optional: tesseract
        apt: %w(antiword catdoc html2text libencode-perl liberror-perl libfile-which-perl libmodule-pluggable-perl libspreadsheet-parsexlsx-perl libspreadsheet-xlsx-perl odt2txt poppler-utils),
      },
      SubscribePlugin: {
        apt: %w(libjson-xs-perl),
        foswiki: %w(MailerContrib),
      },
      TinyMCEPlugin: {
        foswiki: %w(WysiwygPlugin JQueryPlugin),
      },
      TopicInteractionPlugin: {
        apt: %w(libarchive-zip-perl libjson-xs-perl),
        foswiki: %w(FilterPlugin FlexFormPlugin JQueryPlugin MimeIconPlugin RenderPlugin ZonePlugin),
      },
      WebFontsContrib: {
        foswiki: %w(JQueryPlugin),
      },
      WebLinkPlugin: {
        apt: %w(libencode-perl libhtml-html5-entities-perl libhtml-parser-perl),
        foswiki: %w(TopicTitlePlugin),
      },
      XSendFileContrib: {
        apt: %w(libfile-mmagic-xs-perl),
      },
    }.freeze

    INDEPENDENT_PLUGINS = %w(AutoTemplatePlugin AutoViewTemplatePlugin BehaviourContrib CommentPlugin CompareRevisionsAddOn CopyContrib EasyMacroPlugin EditTablePlugin ExternalLinkPlugin FamFamFamContrib FilterPlugin FlexFormPlugin GridLayoutPlugin HistoryPlugin HomePagePlugin InterwikiPlugin MailerContrib MimeIconPlugin PatchFoswikiContrib PreferencesPlugin PubLinkFixupPlugin RenderFormPlugin RenderListPlugin SecurityHeadersPlugin SlideShowPlugin SmiliesPlugin SpreadSheetPlugin TablePlugin TipsContrib TopicTitlePlugin TopicUserMappingContrib TrashPlugin TreeBrowserPlugin TWikiCompatibilityPlugin TwistyPlugin UpdatesPlugin ZonePlugin).freeze

    module_function

    # Return all apt packages needed for Foswiki
    def foswiki_apt_dependencies
      %w(perl perl-doc diffutils grep rcs libalgorithm-diff-perl libalgorithm-diff-xs-perl libauthen-sasl-perl libcgi-pm-perl libcgi-session-perl libcrypt-passwdmd5-perl libdbd-mariadb-perl libdigest-sha-perl libemail-address-perl libemail-address-xs-perl libemail-mime-perl libencode-perl liberror-perl libfile-copy-recursive-perl libhtml-parser-perl libhtml-tree-perl libio-socket-ip-perl libio-socket-ssl-perl libjson-perl libjson-xs-perl liblocale-codes-perl liblocale-maketext-lexicon-perl liblocale-msgfmt-perl liblwp-protocol-https-perl liburi-perl libversion-perl libwww-perl
      )
    end

    # Check if a plugin is independent (does not have any dependencies)
    def foswiki_plugin_is_independent?(plugin)
      INDEPENDENT_PLUGINS.include?(plugin)
    end

    # Check if a plugin is obsolete
    def foswiki_plugin_is_obsolete?(plugin, version = nil)
      version ||= node['foswiki']['version']
      return true if plugin.eql?('SetVariablePlugin') &&
                     Gem::Version.new(version) >=
                     Gem::Version.new('1.2')
      return true if plugin.eql?('TopicTitlePlugin') &&
                     Gem::Version.new(version) >=
                     Gem::Version.new('2.2')
      return true if plugin.eql?('ZonePlugin') &&
                     Gem::Version.new(version) >=
                     Gem::Version.new('1.1')
      false
    end

    # Get all plugins needed for a set of plugins
    def foswiki_plugin_dependencies(plugins)
      return DEPENDENCIES[plugins] if plugins.is_a? Symbol
      return DEPENDENCIES[plugins.to_sym] if plugins.is_a? String

      dependencies = {}
      (plugins || []).each do |plugin|
        (DEPENDENCIES[plugin.to_sym] || {}).each do |system, deps|
          dependencies[system] = [] unless dependencies.include?(system)
          dependencies[system] += deps
          dependencies[system].sort!
        end
      end
      dependencies
    end
  end
end
