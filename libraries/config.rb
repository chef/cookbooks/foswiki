module Foswiki
  # Configuration functions for the Foswiki cookbook
  module Config
    include Chef::Mixin::ShellOut

    # Return the current configuration for the given parameters
    def foswiki_config(config, tools_dir = nil)
      getcfg = if config.is_a? Hash
                 foswiki_flat_config(config).keys
               elsif config.is_a? String
                 [config]
               else
                 config
               end
      return {} if getcfg.empty?

      tools_dir ||= Foswiki::Helper.foswiki_dir('tools', node)
      return {} unless ::File.exist? "#{tools_dir}/configure"

      getcfg.map! { |c| c[0] == '{' ? c : "{#{c}}" }
      getcfg = getcfg.map { |key| "-getcfg '#{key}'" }.join(' ')
      command = "perl -CAS #{tools_dir}/configure -json #{getcfg} 2>&1"

      config = shell_out!(command).stdout
      config.sub!(/^[^{]*/, '')

      # even if we ask for -json ./configure returns a perl hash
      if config.delete("\n") =~ /^{.*};\s*$/
        # tools/configure returns a serialised perl hash,
        # which is very similiar to a ruby hash
        config.gsub!(/\bundef\b/, 'nil')
        config = eval config
        # 'changes' is always returned but not usefull here
        config.delete 'changes'
        config
      else
        Chef::Log.error "#{tools_dir}/configure:\n#{config}"
      end
    end

    # Flatten the keys of a hash, e.g. ['Dir']['File'] => '{Dir}{File}'
    def foswiki_flat_config(config, prefix = '')
      return {} if config.nil? || config.empty?

      flattened = {}
      config.each do |key, value|
        if value.is_a? Hash
          flattened.merge!(foswiki_flat_config(value, prefix + "{#{key}}"))
        else
          key = "{#{key}}" unless key =~ /^{.*}$/
          value = 0 if value == false
          value = 1 if value == true
          value = Foswiki::Helper.foswiki_secret(value)
          flattened["#{prefix}#{key}"] = value
        end
      end
      flattened
    end

    # Get a single Foswiki parameter
    def foswiki_get(key, tools_dir = nil)
      key = "{#{key}}" unless key[0] == '{'
      value = foswiki_config(key, tools_dir)
      key[1..-2].split('}{').each do |param|
        value = value[param]
      end
      value
    end

    def foswiki_resolve(value, tools_dir = nil)
      return if value.nil?
      while value =~ /\$Foswiki::cfg{/
        value = value.gsub(/\$Foswiki::cfg({[A-Za-z}{]+})/) do |_|
          foswiki_get(Regexp.last_match[1], tools_dir)
        end
      end
      value
    end
  end
end

Chef::DSL::Recipe.include Foswiki::Config
Chef::Resource.include Foswiki::Config
