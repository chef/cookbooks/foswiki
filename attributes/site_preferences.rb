# Configuration for Main.SitePreferences
# Default values depend on the version of Foswiki

default['foswiki']['site_preferences']['ALLOWTOPICCHANGE'] = 'AdminGroup'
default['foswiki']['site_preferences']['ALLOWTOPICRENAME'] = 'AdminGroup'

@version = Gem::Version.new(node['foswiki']['version'])

if @version >= Gem::Version.new('1.1.0')
  default['foswiki']['site_preferences']['WIKILOGOIMG'] =
    '%PUBURL%/%SYSTEMWEB%/ProjectLogos/foswiki-logo.gif'
  default['foswiki']['site_preferences']['WIKILOGOALT'] =
    'Powered by Foswiki, The Free and Open Source Wiki'
  default['foswiki']['site_preferences']['WIKILOGOURL'] =
    '%SCRIPTURL{"view"}%/%USERSWEB%/%HOMETOPIC%'
  default['foswiki']['site_preferences']['WIKITOOLNAME'] = 'Foswiki'
  default['foswiki']['site_preferences']['FAVICON'] =
    '%PUBURLPATH%/%SYSTEMWEB%/ProjectLogos/favicon.ico'
  default['foswiki']['site_preferences']['WEBHEADERART'] =
    '%PUBURLPATH%/%SYSTEMWEB%/PatternSkin/header5.gif'
  default['foswiki']['site_preferences']['WEBHEADERBGCOLOR'] = '#ffffff'
  default['foswiki']['site_preferences']['FINALPREFERENCES'] = %w(
    ATTACHFILESIZELIMIT PREVIEWBGIMAGE WIKITOOLNAME WIKIHOMEURL
    ALLOWROOTCHANGE DENYROOTCHANGE USERSWEB SYSTEMWEB DOCWEB
  )
  if @version >= Gem::Version.new('1.2.0')
    default['foswiki']['site_preferences']['WIKILOGOIMG'] =
      '%PUBURL%/%SYSTEMWEB%/ProjectLogos/foswiki-logo.png'
    if @version >= Gem::Version.new('2.1.3')
      default['foswiki']['site_preferences']['FINALPREFERENCES'] = %w(
        ATTACHFILESIZELIMIT PREVIEWBGIMAGE WIKITOOLNAME WIKIHOMEURL
        ALLOWROOTCHANGE DENYROOTCHANGE DOCWEB WIKIWEBMASTER WIKIWEBMASTERNAME
        WIKIAGENTEMAIL WIKIAGENTNAME
      )
    elsif @version >= Gem::Version.new('2.0.1')
      default['foswiki']['site_preferences']['FINALPREFERENCES'] = %w(
        ATTACHFILESIZELIMIT PREVIEWBGIMAGE WIKITOOLNAME WIKIHOMEURL
        ALLOWROOTCHANGE DENYROOTCHANGE DOCWEB
      )
    end
  end
elsif @version >= Gem::Version.new('1.0.3')
  default['foswiki']['site_preferences']['FINALPREFERENCES'] = %w(
    ATTACHFILESIZELIMIT PREVIEWBGIMAGE WIKITOOLNAME WIKIHOMEURL ALLOWROOTCHANGE
    DENYROOTCHANGE USERSWEB SYSTEMWEB DOCWEB
  )
elsif @version >= Gem::Version.new('1.0.0')
  default['foswiki']['site_preferences']['FINALPREFERENCES'] = %w(
    ATTACHFILESIZELIMIT PREVIEWBGIMAGE WIKITOOLNAME WIKIHOMEURL ALLOWROOTCHANGE
    DENYROOTCHANGE FOSWIKI_LAYOUT_URL FOSWIKI_STYLE_URL FOSWIKI_COLORS_URL
    USERSWEB SYSTEMWEB DOCWEB
  )
end
