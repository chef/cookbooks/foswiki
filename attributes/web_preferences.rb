# Configuration for System.WebPreferences
# Default values depend on the version of Foswiki

default['foswiki']['web_preferences']['System']['ALLOWTOPICCHANGE'] =
  '%USERSWEB%.AdminGroup'
default['foswiki']['web_preferences']['System']['ALLOWWEBCHANGE'] =
  '%USERSWEB%.AdminGroup'
default['foswiki']['web_preferences']['System']['ALLOWWEBRENAME'] =
  '%USERSWEB%.AdminGroup'
default['foswiki']['web_preferences']['System']['FINALPREFERENCES'] = %w(
  NOSEARCHALL ATTACHFILESIZELIMIT WIKIWEBMASTER WEBCOPYRIGHT WEBTOPICLIST
  DENYWEBVIEW ALLOWWEBVIEW DENYWEBCHANGE ALLOWWEBCHANGE ALLOWWEBRENAME
  DENYWEBRENAME
)
default['foswiki']['web_preferences']['System']['NOAUTOLINK'] = 0
default['foswiki']['web_preferences']['System']['SITEMAPLIST'] = 'on'
default['foswiki']['web_preferences']['System']['SITEMAPUSETO'] =
  '...discover Foswiki details, and how to start your own site.'
default['foswiki']['web_preferences']['System']['SITEMAPWHAT'] =
  '[[%WEB%.WelcomeGuest][Welcome]], [[%WEB%.UserRegistration][Registration]]' \
  ', and other %WEB%.StartingPoints; Foswiki history &amp; Wiki style; All ' \
  'the docs...'
default['foswiki']['web_preferences']['System']['TOC_HIDE_IF_INCLUDED'] = 'on'
default['foswiki']['web_preferences']['System']['TOC_MIN_DEPTH'] = 2
default['foswiki']['web_preferences']['System']['TOC_TITLE'] = 'On this page:'
default['foswiki']['web_preferences']['System']['WEBBGCOLOR'] = '#FFD8AA'
default['foswiki']['web_preferences']['System']['WEBCOPYRIGHT'] =
  '<span class="foswikiRight"> <a href="http://foswiki.org/"><img ' \
  'src="%PUBURLPATH%/%SYSTEMWEB%/ProjectLogos/foswiki-badge.png" alt="This ' \
  'site is powered by Foswiki" title="This site is powered by Foswiki" ' \
  'border="0" /></a></span>%MAKETEXT{"Copyright &&copy; by the contributing ' \
  'authors. All material on this site is the property of the contributing ' \
  'authors." args="1999-%GMTIME{$year}%"}% <br /> %MAKETEXT{"Ideas, ' \
  'requests, problems regarding [_1]? <a href=\'[_2]\'>Send feedback</a>" ' \
  'args="<nop>%WIKITOOLNAME%,mailto:%WIKIWEBMASTER%?subject=%WIKITOOLNAME%%20' \
  'Feedback%20on%20%BASEWEB%.%BASETOPIC%"}% %IF{"$ WEB= $ SYSTEMWEB" then=""}%'
default['foswiki']['web_preferences']['System']['WEBFORMS'] =
  'DefaultPreferencesForm'
default['foswiki']['web_preferences']['System']['WEBTOPICLIST'] =
  '[[WelcomeGuest][Welcome]] %SEP% [[UserRegistration][Register]] %SEP% ' \
  '[[WebChanges][Changes]] %SEP% [[WebTopicList][Topics]] %SEP% ' \
  '[[WebIndex][Index]] %SEP% [[WebSearch][Search]] %SEP% Go <input ' \
  'type="text" name="topic" size="16" />'

@version = Gem::Version.new(node['foswiki']['version'])

if @version >= Gem::Version.new('1.0.3')
  default['foswiki']['web_preferences']['System']['WEBCOPYRIGHT'] =
    '<span class="foswikiRight"> <a href="http://foswiki.org/"><img ' \
    'src="%PUBURLPATH%/%SYSTEMWEB%/ProjectLogos/foswiki-badge.gif" alt="This ' \
    'site is powered by Foswiki" title="This site is powered by Foswiki" ' \
    'border="0" /></a></span>%MAKETEXT{"Copyright &&copy; by the ' \
    'contributing authors. All material on this site is the property of the ' \
    'contributing authors." args="1999-%GMTIME{$year}%"}% <br /> %MAKETEXT{"' \
    'Ideas, requests, problems regarding [_1]? <a href=\'[_2]\'>Send ' \
    'feedback</a>" args="<nop>%WIKITOOLNAME%,mailto:%WIKIWEBMASTER%?subject=' \
    '%WIKITOOLNAME%%20Feedback%20on%20%BASEWEB%.%BASETOPIC%"}% %IF{"$ WEB= ' \
    '$ SYSTEMWEB" then=""}%'

  if @version >= Gem::Version.new('1.1.0')
    default['foswiki']['web_preferences']['System']['FINALPREFERENCES'] = nil
    default['foswiki']['web_preferences']['System']['NOAUTOLINK'] = nil
    default['foswiki']['web_preferences']['System']['SITEMAPUSETO'] = nil
    default['foswiki']['web_preferences']['System']['SITEMAPWHAT'] = nil
    default['foswiki']['web_preferences']['System']['WEBFORMS'] = %w(
      DefaultPreferencesForm FAQForm
    )
    default['foswiki']['web_preferences']['System']['WEBSUMMARY'] =
      '%MAKETEXT{"Discover the details, and how to start your own site with ' \
      'Foswiki"}% - %MAKETEXT{"The Free and Open Source Wiki."}%'

    if @version >= Gem::Version.new('2.1.1')
      default['foswiki']['web_preferences']['System']['WEBCOPYRIGHT'] =
        '<span class="foswikiRight"> <a href="https://foswiki.org/"><img ' \
        'src="%PUBURLPATH%/%SYSTEMWEB%/ProjectLogos/foswiki-badge.png" ' \
        'height="42" alt="This site is powered by Foswiki" title="This ' \
        'site is powered by Foswiki" /></a></span>%MAKETEXT{"Copyright ' \
        '&&copy; by the contributing authors. All material on this site ' \
        'is the property of the contributing authors." args="1999-%GMTIME' \
        '{$year}%"}% <br /> %MAKETEXT{"Ideas, requests, problems ' \
        'regarding [_1]? <a href=\'[_2]\'>Send feedback</a>" args="<nop>' \
        '%WIKITOOLNAME%,mailto:%WIKIWEBMASTER%?subject=%WIKITOOLNAME%%20' \
        'Feedback%20on%20%BASEWEB%.%BASETOPIC%"}% %IF{"$ WEB= $ ' \
        'SYSTEMWEB" then=""}%'
    end

    if @version >= Gem::Version.new('1.2.0')
      default['foswiki']['web_preferences']['System']['NOAUTOLINK'] = 'off'
    end
  end
end
