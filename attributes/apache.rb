# Apache configuration for the default Foswiki installation
default['foswiki']['apache']['access_control'] = 'System/ResetPassword'
default['foswiki']['apache']['blocked_agents'] = %w(^$ ^Accoona ^ActiveAgent ^Attache BecomeBot ^bot Charlotte/ ^ConveraCrawler ^CrownPeak-HttpAgent ^EmailCollector ^EmailSiphon ^e-SocietyRobot ^Exabot ^FAST ^FDM ^GetRight/6.0a ^GetWebPics ^Gigabot ^gonzo1 ^Google\sSpider ^ichiro ^ie_crawler ^iGetter ^IRLbot Jakarta ^Java ^KrakSpider ^larbin ^LeechGet ^LinkWalker ^Lsearch ^Microsoft MJ12bot MSIECrawler ^MSRBOT ^noxtrumbot ^NutchCVS ^RealDownload ^Rome ^Roverbot ^schibstedsokbot SemrushBot ^Seekbot ^SiteSnagger ^SiteSucker ^Snapbot ^sogou ^SpiderKU ^SpiderMan ^Squid ^Teleport ^User-Agent\: VoilaBot ^voyager ^w3search ^Web\sDownloader ^WebCopier ^WebDevil ^WebSec ^WebVac ^Webwhacker ^Webzip ^Wells ^WhoWhere www\.netforex\.org ^WX_mail ^yacybot ^ZIBB)
default['foswiki']['apache']['blocked_ips'] = [] # regexes for full IPs
default['foswiki']['apache']['cert_ca_path'] = nil # SSLCACertificatePath
default['foswiki']['apache']['cert_chain_file'] = nil
default['foswiki']['apache']['cert_file'] = nil
default['foswiki']['apache']['cert_key_file'] = nil
default['foswiki']['apache']['custom_log'] = 'combined'
default['foswiki']['apache']['favicon'] = '/System/ProjectLogos/favicon.ico'
default['foswiki']['apache']['fcgid_max_request_len'] = 31_457_280 # 30MiB
default['foswiki']['apache']['http_port'] = 80
default['foswiki']['apache']['https_port'] = 443
default['foswiki']['apache']['log_level'] = "info#{' ssl:warn' if node['foswiki']['apache']['use_tls']}"
default['foswiki']['apache']['protect_attachments'] = true
default['foswiki']['apache']['pub_expires_match'] = '\.(jpe?g|gif|png|css(\.gz)?|js(\.gz)?|ico)$'
default['foswiki']['apache']['pub_expires_time'] = nil # e.g. "access plus 11 days"
default['foswiki']['apache']['server_admin'] = "webmaster@#{node['fqdn']}"
default['foswiki']['apache']['server_alias'] = []
default['foswiki']['apache']['server_name'] = node['fqdn']
# Regular expressions for attachments which should be ignored when protect_attachments is set
# :default means [^/]+\.(gif|jpe?g|ico)$, System/(.*)$ and ([^/]+/)+WebPreferences/([^/]+)$
default['foswiki']['apache']['unprotected_attachments'] = :default
default['foswiki']['apache']['use_fcgi'] = false # true if FastCGIEngineContrib is installed
default['foswiki']['apache']['use_short_urls'] = true
default['foswiki']['apache']['use_tls'] = false
default['foswiki']['apache']['use_xsendfile'] = false # true if XSendFileContrib is installed
