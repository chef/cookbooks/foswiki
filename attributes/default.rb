# Attributes for default Foswiki installation
default['foswiki']['admin_group'] = []
default['foswiki']['download_url'] = nil
default['foswiki']['extensions_web'] = nil
default['foswiki']['group'] = nil # Apache default
default['foswiki']['install_dir'] = '/srv/foswiki'
default['foswiki']['plugins'] = []
default['foswiki']['robots']['crawl_delay'] = 5
default['foswiki']['robots']['disallow'] = []
default['foswiki']['robots']['disallow_agents'] = []
default['foswiki']['upgrade'] = false # upgrade Foswiki
default['foswiki']['user'] = nil # Apache default
default['foswiki']['version'] = '2.1.8'

# Attributes used by a Cron job
default['foswiki']['cron']['actionnotify_entries'] = []
default['foswiki']['cron']['entries'] = []
default['foswiki']['cron']['mailnotify_time'] = nil
default['foswiki']['cron']['mailto'] = 'root'
default['foswiki']['cron']['mailto'] = 'root'
default['foswiki']['cron']['statistics_time'] = '0 0 * * *'
default['foswiki']['cron']['tick_time'] = '0 0 * * 0'
default['foswiki']['cron']['tidy_page_cache_max_age'] = '1 year'
default['foswiki']['cron']['tidy_page_cache_time'] = nil
default['foswiki']['cron']['trash_cleanup_time'] = nil

# Attributes set by tools/configure
default['foswiki']['config']['Cache']['DBI']['TablePrefix'] = 'foswiki_cache'
default['foswiki']['config']['Cache']['Enabled'] = false
default['foswiki']['config']['Cache']['Implementation'] = 'Foswiki::PageCache::DBI::Generic'
default['foswiki']['config']['DataDir'] = '{install}/data'
default['foswiki']['config']['DefaultUrlHost'] = 'http://localhost'
default['foswiki']['config']['Htpasswd']['FileName'] =
  '$Foswiki::cfg{DataDir}/.htpasswd'
default['foswiki']['config']['Htpasswd']['GlobalCache'] = false
default['foswiki']['config']['LocalesDir'] = '{install}/locale'
default['foswiki']['config']['Log']['Dir'] = '$Foswiki::cfg{WorkingDir}/logs'
default['foswiki']['config']['PubDir'] = '{install}/pub'
default['foswiki']['config']['PubUrlPath'] = '/pub'
default['foswiki']['config']['ScriptDir'] = '{install}/bin'
default['foswiki']['config']['ScriptSuffix'] = ''
default['foswiki']['config']['ScriptUrlPath'] = '/bin'
default['foswiki']['config']['ScriptUrlPaths']['view'] = ''
default['foswiki']['config']['Store']['Encoding'] = ''
default['foswiki']['config']['Store']['Implementation'] =
  'Foswiki::Store::PlainFile'
default['foswiki']['config']['Store']['SearchAlgorithm'] =
  'Foswiki::Store::SearchAlgorithms::Forking'
default['foswiki']['config']['TemplateDir'] = '{install}/templates'
default['foswiki']['config']['ToolsDir'] = '{install}/tools'
default['foswiki']['config']['WorkingDir'] = '{install}/working'

# Unofficial patches
default['foswiki']['patches']['holidaylistplugin'] = false
default['foswiki']['patches']['noproxy'] = false

# Contents of topics
default['foswiki']['topics'] = {}
